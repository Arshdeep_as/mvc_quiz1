<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Route::get('/', function () {
	$title = 'Home';
    return view('welcome', compact('title'));
});
/*
Route::get('/about', function () {
	$title = 'About';
    return view('welcome', compact('title'));
});

Route::get('/support', function () {
	$title = 'Support';
    return view('welcome', compact('title'));
});

Route::get('/services', function () {
	$title = 'Services';
    return view('welcome', compact('title'));
});

Route::get('/contact', function () {
	$title = 'Contact';
    return view('welcome', compact('title'));
});

*/

Route::get('/{request}', function ($request) {
	$title = $request;
    return view('welcome', compact('title'));
});

